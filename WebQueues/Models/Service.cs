﻿using System;

namespace WebQueues.Models
{
    public class Service
    {
        public Int64 ServiceId { get; set; }
        public String Description { get; set; }
    }
}