﻿using System;

namespace WebQueues.Models
{
    public class Client
    {
        public Int64 ClientId { get; set; }
        public String Name { get; set; }
    }
}