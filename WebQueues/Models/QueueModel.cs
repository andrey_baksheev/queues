﻿namespace WebQueues.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Data.Entity.Infrastructure;

    public class QueueModel : DbContext
    {
        // Контекст настроен для использования строки подключения "QueueModel" из файла конфигурации  
        // приложения (App.config или Web.config). По умолчанию эта строка подключения указывает на базу данных 
        // "WebQueues.Models.QueueModel" в экземпляре LocalDb. 
        // 
        // Если требуется выбрать другую базу данных или поставщик базы данных, измените строку подключения "QueueModel" 
        // в файле конфигурации приложения.
        public QueueModel()
            : base("name=QueueModel")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>().ToTable("tClients");
            modelBuilder.Entity<Client>().HasKey(p => p.ClientId);
            modelBuilder.Entity<Client>().Property(p => p.Name).HasColumnName("Name");
            modelBuilder.Entity<Client>().Property(p => p.Name).IsRequired();
            modelBuilder.Entity<Client>().Property(p => p.Name).HasMaxLength(256);
            modelBuilder.Entity<Client>().Property(p => p.Name).IsUnicode(true);            

            modelBuilder.Entity<Service>().ToTable("tServices");
            modelBuilder.Entity<Service>().HasKey(p => p.ServiceId);
            modelBuilder.Entity<Service>().Property(p => p.Description).HasColumnName("Description");
            modelBuilder.Entity<Service>().Property(p => p.Description).IsRequired();
            modelBuilder.Entity<Service>().Property(p => p.Description).HasMaxLength(256);
            modelBuilder.Entity<Service>().Property(p => p.Description).IsUnicode(true);

            modelBuilder.Entity<Queue>().ToTable("tQueues");

            base.OnModelCreating(modelBuilder);
        }

        // Добавьте DbSet для каждого типа сущности, который требуется включить в модель. Дополнительные сведения 
        // о настройке и использовании модели Code First см. в статье http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Queue> Queues { get; set; }
    }
}