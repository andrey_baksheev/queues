﻿using System;

namespace WebQueues.Models
{
    public class Queue
    {
        public Int64 ServiceId { get; set; }
        public Int64 ClientId { get; set; }
    }
}