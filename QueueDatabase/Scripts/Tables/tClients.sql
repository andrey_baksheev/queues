CREATE TABLE tClients
(
	ClientId bigint IDENTITY (1,1) not null 
	,Name nvarchar(256) not null
	,CONSTRAINT PK_tClients_ClientId PRIMARY KEY CLUSTERED (ClientId)  
	,CONSTRAINT UQ_tClients_Name UNIQUE (Name) 
);

