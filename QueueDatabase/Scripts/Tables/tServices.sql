CREATE TABLE tServices
(
	ServiceId bigint IDENTITY (1,1) not null
	,[Description] nvarchar(256) not null
	,CONSTRAINT PK_tServices_ServiceId PRIMARY KEY CLUSTERED (ServiceId)  
	,CONSTRAINT UQ_tServices_Description UNIQUE ([Description]) 
);

