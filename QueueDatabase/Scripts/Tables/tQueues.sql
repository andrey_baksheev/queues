CREATE TABLE tQueues
(
	ServiceId bigint not null
	,ClientId bigint not null
	,CONSTRAINT FK_tQueues_tServices FOREIGN KEY (ServiceId)     
    REFERENCES tServices (ServiceId)     
    ON DELETE NO ACTION    
    ON UPDATE NO ACTION  
	,CONSTRAINT FK_tQueues_tClients FOREIGN KEY (ClientId)     
    REFERENCES tClients (ClientId)     
    ON DELETE NO ACTION    
    ON UPDATE NO ACTION	
);

